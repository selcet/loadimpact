import Component from '@ember/component';

export default Component.extend({
  model: [{
    title: "tastejs/todomvc",
    authors: "manaswinidas and FadySamirSadek",
    repoLink: "tastejs/todomvc",
    description: "Helping you select an MV* framework - Todo apps for React.js, Ember.js, Angular, and many more"
  },{
    title: "emberjs/ember.js",
    authors: "manaswinidas and FadySamirSadek",
    repoLink: "emberjs/ember.js",
    description: "Ember.js - A JavaScript framework for creating ambitious web applications"
  },{
    title: "text-mask/text-mask",
    authors: "lozjackson",
    repoLink: "text-mask/text-mask",
    description: "Input mask for React, Angular, Ember, Vue, & plain JavaScript"
  },{
    title: "HospitalRun/hospitalrun-frontend",
    authors: "tangollama",
    repoLink: "HospitalRun/hospitalrun-frontend",
    description: "Ember front end for HospitalRun"
  },{
    title: "ember-cli/ember-cli",
    authors: "rwjblue",
    repoLink: "ember-cli/ember-cli",
    description: "The Ember.js command line utility"
  },{
    title: "emberjs/data",
    authors: "rwjblue",
    repoLink: "emberjs/data",
    description: "A data persistence library for Ember.js."
  },{
    title: "cowbell/sharedrop",
    authors: "szimek",
    repoLink: "cowbell/sharedrop",
    description: "HTML5 clone of Apple's AirDrop - easy P2P file transfer powered by WebRTC"
  },{
    title: "simplabs/ember-simple-auth",
    authors: "jessica-jordan",
    repoLink: "simplabs/ember-simple-auth",
    description: "A library for implementing authentication/authorization in Ember.js applications."
  },{
    title: "Addepar/ember-table",
    authors: "bgentry and pzuraq",
    repoLink: "Addepar/ember-table",
    description: "An addon to support large data set and a number of features around table."
  }]
});
